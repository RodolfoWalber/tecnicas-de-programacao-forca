#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jogador.h"

struct Jogador *criaJogador(char *nome) ///construtor
{
    struct Jogador *jogador = malloc(sizeof(struct Jogador));
    if(jogador == NULL)
    {
        printf("Memoria nao alocada P.P\n");
        printf("Funcao - criaJogador\n");
        exit(1);
    }

    jogador->nome = strdup(nome);
    jogador->vitorias = 0;
    jogador->derrotas = 0;
    jogador->tentativas = 3;
    return jogador;
}

void imprimiJogador(struct Jogador *jogador)    ///imprimi status do jogador
{
    printf("Nome do jogador: %s\n",jogador->nome);
    printf("Numero de vitorias: %d\n", jogador->vitorias);
    printf("Numero de derrotas: %d\n", jogador->derrotas);
    printf("Vidas restantes: %d\n", jogador->tentativas);
}

void adicionaVitoria(struct Jogador *jogador)
{
    jogador->vitorias++;
}

void adicionaDerrota(struct Jogador *jogador)
{
    jogador->derrotas++;
}

void adicionaTentativa(struct Jogador *jogador)
{
    jogador->tentativas++;
}

void removeTentativa(struct Jogador *jogador)
{
    jogador->tentativas--;
}
