#ifndef HEADER_LEITURAEARMAZENAMENTO
#define HEADER_LEITURAEARMAZENAMENTO

FILE *abreArquivo(char arquivoEndereco[]);

void fechaArquivo(FILE *arquivo_ptr);

void escolhePalavraAleatoriamente(char *texto,FILE *arquivo_ptr, int contador);

#endif
