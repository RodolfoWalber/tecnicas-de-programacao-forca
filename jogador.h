#ifndef HEADER_JOGADOR
#define HEADER_JOGADOR

struct Jogador
{
    char *nome;
    int vitorias;
    int derrotas;
    int tentativas;
};

struct Jogador *criaJogador(char *nome); ///construtor

void imprimiJogador(struct Jogador *jogador);

void adicionaVitoria(struct Jogador *jogador);

void adicionaDerrota(struct Jogador *jogador);

void adicionaTentativa(struct Jogador *jogador);

void removeTentativa(struct Jogador *jogador);

#endif
