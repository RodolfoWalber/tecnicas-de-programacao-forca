#ifndef HEADER_PALAVRA
#define HEADER_PALAVRA

struct Palavra
{
    char *palavra;
    int comprimento;
};

struct Palavra *criaPalavra(char *palavra);  ///construtor

void preencheNulo(char *palavra, int comprimento);  ///preenche string com o caracter nulo '\0'

#endif
