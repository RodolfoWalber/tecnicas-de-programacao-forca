#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "leituraEArmazenamento.h"

FILE *abreArquivo(char arquivoEndereco[])
{
    FILE *temp_ptr = NULL;
    /**
    fopen - abre o arquivo cujo nome est� especificado no primeiro par�metro filename/pathname e o associa a um fluxo (stream) que pode ser identificado em opera��es futuras
    pela atribuicao a variavel ponteiro de arquivo (FILE)
    */
    temp_ptr = fopen(arquivoEndereco,"r");
    if(temp_ptr == NULL)
    {
        printf("Arquivo nao aberto");
        exit(1);
    }
    /**
    referencia do uso de fscanf. Uma das diferencas para o fgets eh a leitura ate o espaco em branco, sem guarda-lo.
    Pode-se limitar o numero de caracteres lidos especificando no formato "%xs" onde x eh o numero de caracteres maximo lido
    while(fscanf(arquivo_ptr,"%s",texto) !=EOF)
    {
        printf("%s\n",texto);

    }
    */
    return temp_ptr;
}

void fechaArquivo(FILE *arquivo_ptr)
{
    fclose(arquivo_ptr);
}

void escolhePalavraAleatoriamente(char *texto,FILE *arquivo_ptr, int contador)  ///recebe valor aleatorio, escolhe do arquivo a palavra, contando as linhas
{
    int i = 0;
    while((fgets(texto, 999, arquivo_ptr) !='\0') && (i <= contador))
    {
        i++;
    }
    if ((strlen(texto) > 0) && (texto[strlen (texto) - 1] == '\n'))    ///retira nova linha do final da string
    {
        texto[strlen (texto) - 1] = '\0';
    }
}
