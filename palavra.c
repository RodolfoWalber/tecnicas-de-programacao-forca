#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "palavra.h"

struct Palavra *criaPalavra(char *palavra)  ///construtor
{
    struct Palavra *palavraForca = malloc(sizeof(struct Palavra));
    if(palavraForca == NULL)
    {
        printf("Memoria nao alocada O.O\n");
        printf("Funcao - criaPalavra\n");
        exit(1);
    }
    /**
    strdup - retorna um ponteiro para uma nova string, sendo uma replica para a string anterior. Abaixo vemos o passo a passo da funcao.
    palavraForca->palavra = malloc(strlen(palavra)+1);
    strcpy(palavraForca->palavra, palavra);
    */
    palavraForca->palavra = strdup(palavra);
    palavraForca->comprimento = strlen(palavra);
    return palavraForca;

}

void preencheNulo(char *palavra, int comprimento)
{
    for(int i = 0; i < (comprimento + 1); i++)
    {
        palavra[i]='\0';
    }
};
