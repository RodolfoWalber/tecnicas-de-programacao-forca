/**
REFERENCIA
gerenciamento de string - https://github.com/angrave/SystemProgramming/wiki/C-Programming,-Part-4:-Strings-and-Structs
biblioteca string prototipos - http://www.cplusplus.com/reference/cstring/
wordlist - https://github.com/manashmandal/Hangman-in-C/blob/master/wordlist
*/
/**
TODO
-perguntar numero de jogares
*/
/**
WISHLIST
-dificuldade de jogos, altera a quantidade de tentativas do jogador e/ou limita tamanho de caracteres da palavra
-protecao contra numeros e caracteres especiais digitados pelo jogador
-tratar letras maiusculas
-adicionar convencao nas variaveis do tipo ponteiro: nomeDaVariavel_Ptr, ptr_nomeDaVariavel
-adicionar dicas conforme dificuldade
-desacoplar estruturas de controle e gerenciamento do main em outro arquivo e header
-salvar profile do jogador em arquivo e carregar em outra execucao
-carregar txt de palavras em lista(?) com informacoes necessarias para selecionar dificuldade/ agrupar em dificuldade?
-mostrar historico/ ranking de jogadores
-funcionalidade roda roda jequiti - multiplayer local
*/
/**
BUG
-qdo o caracter digitado pelo usuario aparecer duas vezes na palavra, a leitura fos caracteres digitados acontece ate o segundo caracter, sendo ignorados os outros se houver.
-apos reiniciar game, depois de uma derrota, qualquer letra faz o jogador perder.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "jogador.h"
#include "palavra.h"
#include "leituraEArmazenamento.h"

#define NOME_CARAC_MAX 256


int main()
{
    printf("Bem Vindo a Forca 2000!\n");

    srand(time(NULL));
    char texto[100];


    char *nome = malloc(NOME_CARAC_MAX);
    if(nome == NULL)
    {
        printf("Memoria nao alocada P.P\n");
        printf("criacao char nome\n");
        exit(1);
    }
    printf("Digite o seu nome:");
    /**
    File Get Stream le caracteres ate o maximo definido, no programa e 256 ou quando uma nova linha e adicionada (guardando a nova linha tambem).
    Dessa forma, nao ha risco de buffer overflow, ou seja, o input passar o tamanho estabelecido na criacao da variavel.
    O ultimo argumento refere-se onde sera feita a leitura do input, no caso e do standard input.
    */
    fgets(nome, NOME_CARAC_MAX, stdin);

    if ((strlen(nome) > 0) && (nome[strlen (nome) - 1] == '\n'))    ///retira nova linha do final da string
    {
        nome[strlen (nome) - 1] = '\0';
    }

    printf("Bom jogo %s e boa sorte!\n", nome);
    struct Jogador *p1 = criaJogador(nome);
    free(nome); ///libera memoria do ponteiro nome
    //imprimiJogador(p1);

    printf("Digite uma letra ou mais letras caso saiba a palavra...\n");
    printf("Cada letra ou tentativa errada ira fazer voce perder uma vida.\n");


    int erro = 1;   ///variavel de controle, inicia supondo que o jogador ira errar a letra
    int jogarNovamente = 1; ///variavel de controle para iniciar outro jogo

    while(jogarNovamente)
    {
    system("cls");  ///limpa console
    imprimiJogador(p1);
    FILE *arquivo_ptr = NULL;
    char arquivoEndereco[] = "palavras.txt";
    arquivo_ptr = abreArquivo(arquivoEndereco);
    int contador = rand()%40;

    escolhePalavraAleatoriamente(texto,arquivo_ptr,contador);
    struct Palavra *palavraAtual = criaPalavra(texto);

    //printf("%s\n",palavraAtual->palavra);

    fechaArquivo(arquivo_ptr);

    char palavraTemp[palavraAtual->comprimento + 1];
    preencheNulo(palavraTemp, palavraAtual->comprimento);
    char temp[palavraAtual->comprimento + 1];

    char caracteresErrados[p1->tentativas + 1];
    preencheNulo(caracteresErrados, p1->tentativas);

    while(1)
        {
            printf("%s\n",palavraAtual->palavra);
        for(int i = 0; i < palavraAtual->comprimento; i++)  ///laco que gera as linhas na tela
        {
            if(palavraTemp[i] == NULL)
            {
            printf("_ ");
            }
            else
            {
                printf("%c ", palavraTemp[i]);
            }
        }
        printf("\n");

        printf("Letras erradas: ");
        for(int i = 0; i <= (p1->tentativas); i++)  ///laco que mostra os caracteres errados
        {
            printf("%c ", caracteresErrados[i]);
        }

        printf("\n");

        preencheNulo(temp,palavraAtual->comprimento + 1);

        fgets(temp, palavraAtual->comprimento + 1, stdin);

        if(temp[0] == '\n') ///protecao para imput de nova linha sem caracter
            {
                printf("Nenhuma letra registrada, tente novamente.\n");
                system("cls");
                continue;
            }
        else if(strcmp(temp, palavraAtual->palavra) == 0)
            {
                printf("Parabens voce ganhou!\n");
                adicionaVitoria(p1);
                break;
            }



        int contador = 0;
        for(int i = 0; i < palavraAtual->comprimento; i++)
        {
            if(temp[contador] == palavraAtual->palavra[i])
            {
                palavraTemp[i] = temp[contador];
                erro = 0;
            }
            else if(i == (palavraAtual->comprimento -1))
            {
                if(erro)
                {
                    caracteresErrados[3 - p1->tentativas] = temp[contador];
                    removeTentativa(p1);
                }
                contador++;
                i = -1;
                erro = 1;
            }
            if(temp[contador] == '\n')
            {
                break;
            }
        }


        if(strcmp(palavraTemp, palavraAtual->palavra) == 0)
        {
            printf("Parabens voce ganhou!\n");
            adicionaVitoria(p1);
            break;
        }
        if(p1->tentativas <= 0)
        {
            printf("Suas tentivas terminaram :(\n");
            break;
        }
        imprimiJogador(p1);
        erro = 1;   ///variavel de controle caso o usuario acerte a primeira letra
    }

    printf("Deseja jogar novamente?\n");
    printf("S - sim\nN - nao\n");
    char tempResposta[NOME_CARAC_MAX];
    scanf("%s",tempResposta);
    printf("carac: %s\n", tempResposta);
    if(strcmp("nao",tempResposta) == 0 || strcmp("n",tempResposta) == 0 || strcmp("Nao",tempResposta) == 0 || strcmp("N",tempResposta) == 0)
    {
        jogarNovamente = 0;
        printf("entrou\n");
        break;
    }


    }

    return 0;
}
