A3 – Avaliação Prática

Descrição: desenvolver um jogo em C que utilize ponteiros e funções. O jogo deverá ser definido pelo grupo. Alguns exemplos são: jogo da velha, jogo de dados, forca, treinamento de tabuada, desafio de perguntas e/ou cálculos, e etc. O grupo deverá descrever no trabalho quais as regras do jogo desenvolvido. Todo o desenvolvimento do jogo deverá ser modularizado, ou seja, organizado em funções. O código-fonte deverá ser totalmente documentado, de forma que qualquer programador possa dar continuidade ao trabalho. Ao entrar no jogo, deverá ser solicitado o número de jogadores participantes. Depois, deverá ser solicitado o nome de cada jogador. Ao final de cada partida, deverá ser exibido na tela o nome do jogador (ou jogadores) e sua respectiva pontuação. Por exemplo, se for o jogo da velha, ao final do jogo deverá ser apresentado o nome do jogador e o número de vitória que possui.

O grupo deve considerar a seguinte questão: se outro grupo tivesse que dar continuidade a este trabalho (com o desenvolvimento de novas funcionalidades), a documentação seria suficiente?  Seria possível outro grupo desenvolver novas funcionalidades?

Forma de entrega e avaliação: O trabalho deverá ser apresentado e entregue (rodando) em sala de aula, ao professor, no dia estipulado no cronograma. Após a apresentação, o grupo deverá criar um arquivo .zip contendo os código-fontes (.c), um arquivo texto com as regras do jogo (.txt) e um arquivo texto explicando como utiliza o jogo. Este arquivo .zip deverá ser postado no blackboard.

Nota: a avaliação será de 0-10. 0 para o trabalho não entregue e 10 para o trabalho que atenda totalmente aos critérios.

Critérios de avaliação:

- trabalho original (não pode ter cópia de outros colegas)
- descrição das regras do jogo
- documentação do código, de forma que qualquer programador possa dar continuidade no desenvolvimento do programa
- documentação das variáveis, funções, vetores, ponteiros e etc
- funcionamento das funções desenvolvidas
- criação de arquivos de cabeçalho (.h)
- modularização do programa
- estrutura do código (lógica, aninhamento, nome de variáveis/funções e etc)
- uso de ponteiros
- execução do código com sucesso
- solicitação e exibição do nome do jogador e pontuação
- explicação por parte do grupo sobre o funcionamento do programa e as funções